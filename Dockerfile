FROM node:10.6-alpine AS base

ENV GATEWAY_PATH=/usr/src/gateway

ENV PORT=3131 \
  HOST=0.0.0.0

EXPOSE $PORT

WORKDIR ${GATEWAY_PATH}

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

CMD [ "yarn", "dev" ];